﻿#ifndef TSIC30_H
#define TSIC30_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "driver/gpio.h"

class Tsic306 {
public:
    Tsic306();
    void start(gpio_num_t rxPin);
    bool getTemperature(float &temp);
private:
    static const int RX_BUF_SIZE = 100;

    enum Symbol {
        SYM_ZERO,
        SYM_ONE,
        SYM_SYNC,
        SYM_ERROR
    };
    bool parseFrame(uint8_t *data, uint8_t *result);
    Symbol parseSymbol(uint8_t data);
    static void rx_task(void *arg);
    float toCelsius(uint16_t data);
    float temperature = 250;

    TickType_t lastReading = 0;
};

#endif // TSIC30_H
