#include "bletask.h"


BleTask *bleTask = nullptr;
#define TAG "BLETASK"

BleTask::BleTask(PidMemory *memory, bool *requestSaveParameters) : memory(memory), requestSaveParameters(requestSaveParameters) {
    bleTask = this;
    memcpy(&gl_profile_tab[PROFILE_INDEX_USERDATA].name, "livestream\0", 11);
    gl_profile_tab[PROFILE_INDEX_USERDATA].gatts_if = ESP_GATT_IF_NONE;
    gl_profile_tab[PROFILE_INDEX_USERDATA].service_id.id.uuid.uuid.uuid16 = GATTS_SERVICE_UUID_USERDATA;
    gl_profile_tab[PROFILE_INDEX_USERDATA].service_id.is_primary = true;
    gl_profile_tab[PROFILE_INDEX_USERDATA].service_id.id.inst_id = 0x00;
    gl_profile_tab[PROFILE_INDEX_USERDATA].service_id.id.uuid.len = ESP_UUID_LEN_16;
    gl_profile_tab[PROFILE_INDEX_USERDATA].service_handle = GATTS_NUM_HANDLE_USERDATA;
    gl_profile_tab[PROFILE_INDEX_USERDATA].numChars = 1;
    gl_profile_tab[PROFILE_INDEX_USERDATA].charInst[0].char_uuid.len = ESP_UUID_LEN_16;
    gl_profile_tab[PROFILE_INDEX_USERDATA].charInst[0].char_uuid.uuid.uuid16 = GATTS_CHAR_UUID_USERDATA;
    gl_profile_tab[PROFILE_INDEX_USERDATA].charInst[0].char_perm = ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE;
    gl_profile_tab[PROFILE_INDEX_USERDATA].charInst[0].char_prop = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
    btRxBuff_h = xRingbufferCreateStatic(BT_RX_RINGBUF_LEN, RINGBUF_TYPE_BYTEBUF, btRxByteBuff, &btRxBuff);
}

bool BleTask::start() {
    esp_err_t ret  = esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT);

    if(ret != ESP_OK && ret != ESP_ERR_INVALID_STATE) {
        ESP_LOGE(TAG, "Error bluetooth memrelease: %s", esp_err_to_name(ret));
    }

    if(esp_bt_controller_get_status() == ESP_BT_CONTROLLER_STATUS_ENABLED) {
        ESP_LOGW(TAG, "Bluetooth already enabled");
        return true;
    }

    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    ret = esp_bt_controller_init(&bt_cfg);

    if(ret) {
        ESP_LOGE(TAG, "%s initialize controller failed code: %s", __func__, esp_err_to_name(ret));
        return false;
    }

    ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);

    if(ret) {
        ESP_LOGE(TAG, "%s enable controller failed: %s", __func__, esp_err_to_name(ret));
        return false;
    }

    ret = esp_bluedroid_init();

    if(ret) {
        ESP_LOGE(TAG, "%s init bluetooth failed: %s", __func__, esp_err_to_name(ret));
        return false;
    }

    ret = esp_bluedroid_enable();

    if(ret) {
        ESP_LOGE(TAG, "%s enable bluetooth failed: %s", __func__, esp_err_to_name(ret));
        return false;
    }

    ret = esp_ble_gatt_set_local_mtu(ESP_GATT_MAX_MTU_SIZE); // 517 bytes

    if(ret) {
        ESP_LOGE(TAG, "set local  MTU failed, error code = %s", esp_err_to_name(ret));
    }

    uint8_t btMacAddress[6] = { 0 };
    uint8_t addrType = 0;
    esp_ble_gap_get_local_used_addr(btMacAddress, &addrType);
    char btName[] = "Silvia BLE";
    ESP_LOGI(TAG, "Set device name: %s", btName);
    esp_err_t set_dev_name_ret = esp_ble_gap_set_device_name(btName);

    if(set_dev_name_ret) {
        ESP_LOGE(TAG, "set device name failed, error code = %x", set_dev_name_ret);
    }

    ret = esp_ble_gatts_register_callback(gatts_event_handler);

    if(ret == ESP_OK) {
        ret = esp_ble_gap_register_callback(gap_event_handler);

        if(ret == ESP_OK) {
            ret = esp_ble_gatts_app_register(PROFILE_INDEX_USERDATA);
        } else {
            ESP_LOGE(TAG, "gap register error, error code = %x", ret);
        }
    } else {
        ESP_LOGE(TAG, "gatts register error, error code = %x", ret);
    }

    ESP_LOGI(TAG, "Bluetooth controller status after register: %d", esp_bt_controller_get_status());
    xTaskCreate(BleTask::rxTask, "BleRxTask", 4 * 1024, this, 3, &rxTask_h);
    xTaskCreate(BleTask::txTask, "BleTxTask", 4 * 1024, this, 3, &txTask_h);
    return ret == ESP_OK;
}

bool BleTask::stop() {
    esp_bluedroid_disable();
    esp_bluedroid_deinit();
    esp_bt_controller_disable();
    esp_bt_controller_deinit();
    ESP_LOGI(TAG, "Bluetooth controller status at %s: %d", __func__, esp_bt_controller_get_status());
    return true;
}

void BleTask::rxTask(void* arg) {
    BleTask *bleTask = reinterpret_cast<BleTask*>(arg);
    uint8_t *inData;
    size_t itemSize;

    while(true) {
        itemSize = 0;
        inData = (uint8_t *)xRingbufferReceive(bleTask->btRxBuff_h, &itemSize, portMAX_DELAY);

        if(itemSize > 0) {
            ESP_LOGI(TAG, "got %i bytes:", itemSize);
            esp_log_buffer_hex("got some", inData, itemSize);

            switch(itemSize) {
                case 28:
                    memcpy(&bleTask->memory->p, &inData[0], 4);
                    memcpy(&bleTask->memory->i, &inData[4], 4);
                    memcpy(&bleTask->memory->d, &inData[8], 4);
                    memcpy(&bleTask->memory->secsPrebrewPump, &inData[12], 4);
                    memcpy(&bleTask->memory->secsPrebrewWait, &inData[16], 4);
                    memcpy(&bleTask->memory->secsBrewAuto, &inData[20], 4);
                    memcpy(&bleTask->memory->setpointBrew, &inData[24], 4);
                    bleTask->sendParameters = true;
                    break;

                case 1:
                    if(inData[0] == 0xA5) {
                        // store parameters
                        *bleTask->requestSaveParameters = true;
                    }

                    break;
            }

            vRingbufferReturnItem(bleTask->btRxBuff_h, inData);
        }
    }
}

void BleTask::txTask(void* arg) {
    BleTask *bleTask = reinterpret_cast<BleTask*>(arg);
    gatts_profile_inst *instance = &bleTask->gl_profile_tab[0];
    uint8_t out[40] = { 0 };
    int packetsFree = 0;

    while(1) {
        if(instance->connected) {
            int index = 0;

            if(bleTask->sendParameters) {
                memcpy(&out[index], &bleTask->memory->p, 4);
                index += 4;
                memcpy(&out[index], &bleTask->memory->i, 4);
                index += 4;
                memcpy(&out[index], &bleTask->memory->d, 4);
                index += 4;
                memcpy(&out[index], &bleTask->memory->secsPrebrewPump, 4);
                index += 4;
                memcpy(&out[index], &bleTask->memory->secsPrebrewWait, 4);
                index += 4;
                memcpy(&out[index], &bleTask->memory->secsBrewAuto, 4);
                index += 4;
                memcpy(&out[index], &bleTask->memory->setpointBrew, 4);
                index += 4;
                bleTask->sendParameters = false;
            }

            memcpy(&out[index], &bleTask->memory->temperature, 4);
            index += 4;
            memcpy(&out[index], &bleTask->memory->output, 4);
            index += 4;

            do {
                packetsFree = esp_ble_get_sendable_packets_num();

                if(packetsFree > 0) {
                    esp_ble_gatts_send_indicate(instance->gatts_if, instance->conn_id, instance->charInst[0].char_handle, index, out, false);
                } else {
                    vTaskDelay(1);
                }
            } while(packetsFree == 0);
        }

        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

void BleTask::gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param) {
    switch(event) {
        case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
            bleTask->adv_config_done &= (~adv_config_flag);

            if(bleTask->adv_config_done == 0) {
                esp_ble_gap_start_advertising(&bleTask->adv_params);
            }

            break;

        case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
            bleTask->adv_config_done &= (~scan_rsp_config_flag);

            if(bleTask->adv_config_done == 0) {
                esp_ble_gap_start_advertising(&bleTask->adv_params);
                bleTask->adv_config_done = 1;
            }

            break;

        case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:

            //advertising start complete event to indicate advertising start successfully or failed
            if(param->adv_start_cmpl.status == ESP_BT_STATUS_SUCCESS) {
                ESP_LOGE(TAG, "Advertising start");
            } else {
                ESP_LOGE(TAG, "Advertising start failed");
            }

            break;

        case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
            if(param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
                ESP_LOGE(TAG, "Advertising stop failed");
            } else {
                ESP_LOGI(TAG, "Advertising stop");
            }

            break;

        case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
            ESP_LOGI(TAG, "update connetion params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
                     param->update_conn_params.status, param->update_conn_params.min_int, param->update_conn_params.max_int, param->update_conn_params.conn_int,
                     param->update_conn_params.latency, param->update_conn_params.timeout);
            break;

        default:
            break;
    }
}
void BleTask::gattsSendResponse(esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    esp_gatt_status_t status = ESP_GATT_OK;

    if(param->write.is_prep) {
        if(param->write.offset > PREPARE_BUF_MAX_SIZE || prepareWriteBuffer.prepare_len > param->write.offset) {
            status = ESP_GATT_INVALID_OFFSET;
        } else if((param->write.offset + param->write.len) > PREPARE_BUF_MAX_SIZE) {
            status = ESP_GATT_INVALID_ATTR_LEN;
        }

        esp_gatt_rsp_t gatt_rsp;
        gatt_rsp.attr_value.len = param->write.len;
        gatt_rsp.attr_value.handle = param->write.handle;
        gatt_rsp.attr_value.offset = param->write.offset;
        gatt_rsp.attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
        memcpy(gatt_rsp.attr_value.value, param->write.value, param->write.len);
        esp_err_t response_err = esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, &gatt_rsp);

        if(response_err != ESP_OK) {
            ESP_LOGE(TAG, "Send response error");
        }

        if(status != ESP_GATT_OK) {
            return;
        }

        memcpy(prepareWriteBuffer.prepare_buf + param->write.offset, param->write.value, param->write.len);
        prepareWriteBuffer.prepare_len += param->write.len;
    } else {
        esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, NULL);
    }
}
void BleTask::gatts_profile_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param, int index) {
    gatts_profile_inst *instance = &bleTask->gl_profile_tab[index];
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    switch(event) {
        case ESP_GATTS_REG_EVT: {
            //ESP_LOGI(BLETASK_TAG, "REGISTER_APP_EVT, status %d, app_id %d", param->reg.status, param->reg.app_id);
            instance->gatts_if = gatts_if;
            //config adv data
            esp_err_t ret = esp_ble_gap_config_adv_data(&adv_data);

            if(ret) {
                ESP_LOGE(TAG, "config adv data failed, error code = %x", ret);
            }

            adv_config_done |= adv_config_flag;
            //config scan response data
            ret = esp_ble_gap_config_adv_data(&scan_rsp_data);

            if(ret) {
                ESP_LOGE(TAG, "config scan response data failed, error code = %x", ret);
            }

            adv_config_done |= scan_rsp_config_flag;
            esp_ble_gatts_create_service(gatts_if, &instance->service_id, instance->service_handle);
            break;
        }

        case ESP_GATTS_READ_EVT:
            ESP_LOGI(TAG, "GATT_READ_EVT, conn_id %d, trans_id %d, handle %d", param->read.conn_id, param->read.trans_id, param->read.handle);
            esp_gatt_rsp_t rsp;
            memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
            rsp.attr_value.handle = param->read.handle;
            rsp.attr_value.len = 10;
            memcpy(rsp.attr_value.value, &instance->name, 15);
            esp_ble_gatts_send_response(gatts_if, param->read.conn_id, param->read.trans_id, ESP_GATT_OK, &rsp);
            break;

        case ESP_GATTS_WRITE_EVT:

            //ESP_LOGI(BLETASK_TAG, "GATT_WRITE_EVT, conn_id %d, trans_id %d, handle %d", param->write.conn_id, param->write.trans_id, param->write.handle);
            if(!param->write.is_prep) {
                //ESP_LOGI(BLETASK_TAG, "GATT_WRITE_EVT, value len %d, value :", param->write.len);
                //esp_log_buffer_hex(BLETASK_TAG, param->write.value, param->write.len);
                if(instance->charInst[0].descr_handle == param->write.handle) {
                    if(param->write.len == 2) {
                        uint16_t descr_value = param->write.value[1] << 8 | param->write.value[0];

                        if(descr_value == 0x0001) {
                            instance->charInst[0].sending = true;
                        } else if(descr_value == 0x0000) {
                            instance->charInst[0].sending = false;
                        } else {
                            ESP_LOGE(TAG, "unknown descr value");
                            esp_log_buffer_hex(TAG, param->write.value, param->write.len);
                        }

                        if(instance->charInst[0].sending) {
                            esp_ble_gap_stop_advertising();
                            instance->connected = true;
                            sendParameters = true;
                            esp_ble_conn_update_params_t conn_params = { { 0 }, 0, 0, 0, 0 };
                            memcpy(conn_params.bda, param->write.bda, sizeof(esp_bd_addr_t));
                            /* For the IOS system, please reference the apple official documents about the ble connection parameters restrictions. */
                            conn_params.latency = 0;
                            conn_params.max_int = 0x20;    // max_int = 0x20*1.25ms = 40ms
                            conn_params.min_int = 0x10;    // min_int = 0x10*1.25ms = 20ms
                            conn_params.timeout = 400;     // timeout = 400*10ms = 4000ms
                            ESP_LOGI(TAG, "service connected: %s conn_id %d, remote address:", instance->name, param->write.conn_id);
                            esp_log_buffer_hex(TAG, param->write.bda, 6);
                            instance->conn_id = param->write.conn_id;
                            //start sent the update connection parameters to the peer device.
                            esp_ble_gap_update_conn_params(&conn_params);
                            ESP_LOGI(TAG, "sending of %s enabled: %s", instance->name, instance->charInst[0].sending ? "true" : "false");
                        }
                    }
                } else {
                    xRingbufferSendFromISR(bleTask->btRxBuff_h, param->write.value, param->write.len, &xHigherPriorityTaskWoken);
                }
            } else {
                ESP_LOGW(TAG, "is prep");
            }

            if(param->write.need_rsp) {
                gattsSendResponse(gatts_if, param);
            }

            break;

        case ESP_GATTS_EXEC_WRITE_EVT:
            ESP_LOGI(TAG, "ESP_GATTS_EXEC_WRITE_EVT");
            esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
            break;

        case ESP_GATTS_MTU_EVT:
            ESP_LOGI(TAG, "ESP_GATTS_MTU_EVT, MTU %d", param->mtu.mtu);
            break;

        case ESP_GATTS_UNREG_EVT:
            ESP_LOGI(TAG, "ESP_GATTS_UNREG_EVT");
            break;

        case ESP_GATTS_CREATE_EVT: {
            //ESP_LOGI(BLETASK_TAG, "CREATE_SERVICE_EVT, status %d,  service_handle %d", param->create.status, param->create.service_handle);
            instance->service_handle = param->create.service_handle;
            esp_ble_gatts_start_service(instance->service_handle);

            for(uint8_t i = 0; i < instance->numChars; i++) {
                esp_err_t add_char_ret = esp_ble_gatts_add_char(instance->service_handle, &instance->charInst[i].char_uuid, instance->charInst[i].char_perm,
                                         instance->charInst[i].char_prop,
                                         NULL, ESP_GATT_RSP_BY_APP);

                if(add_char_ret) {
                    ESP_LOGE(TAG, "add char failed, error code =%x", add_char_ret);
                }
            }

            break;
        }

        case ESP_GATTS_ADD_INCL_SRVC_EVT:
            ESP_LOGI(TAG, "ESP_GATTS_ADD_INCL_SRVC_EVT");
            break;

        case ESP_GATTS_ADD_CHAR_EVT: {
            uint16_t length = 0;
            const uint8_t *prf_char;
            //ESP_LOGI(BLETASK_TAG, "ADD_CHAR_EVT, status %d,  attr_handle %d, service_handle %d", param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle);
            instance->charInst[0].char_handle = param->add_char.attr_handle;
            instance->charInst[0].descr_uuid.len = ESP_UUID_LEN_16;
            instance->charInst[0].descr_uuid.uuid.uuid16 = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;
            esp_err_t get_attr_ret = esp_ble_gatts_get_attr_value(param->add_char.attr_handle, &length, &prf_char);

            if(get_attr_ret == ESP_FAIL) {
                ESP_LOGE(TAG, "ILLEGAL HANDLE");
            }

            for(int i = 0; i < length; i++) {
                ESP_LOGI(TAG, "prf_char[%x] =%x", i, prf_char[i]);
            }

            esp_err_t add_descr_ret = esp_ble_gatts_add_char_descr(instance->service_handle, &instance->charInst[0].descr_uuid, instance->charInst[0].char_perm, NULL,
                                      NULL);

            if(add_descr_ret) {
                ESP_LOGE(TAG, "add char descr failed, error code =%x", add_descr_ret);
            }

            break;
        }

        case ESP_GATTS_ADD_CHAR_DESCR_EVT:
            instance->charInst[0].descr_handle = param->add_char_descr.attr_handle;
            //ESP_LOGI(BLETASK_TAG, "ADD_DESCR_EVT, status %d, attr_handle %d, service_handle %d", param->add_char_descr.status, param->add_char_descr.attr_handle, param->add_char_descr.service_handle);
            break;

        case ESP_GATTS_DELETE_EVT:
            ESP_LOGI(TAG, "ESP_GATTS_DELETE_EVT");
            break;

        case ESP_GATTS_START_EVT:
            ESP_LOGI(TAG, "SERVICE_START_EVT, status %d, service_handle %d", param->start.status, param->start.service_handle);
            break;

        case ESP_GATTS_STOP_EVT:
            ESP_LOGI(TAG, "ESP_GATTS_STOP_EVT");
            break;

        case ESP_GATTS_DISCONNECT_EVT: {
            ESP_LOGI(TAG, "ESP_GATTS_DISCONNECT_EVT: %s", instance->name);
            instance->connected = false;
            esp_err_t result = esp_ble_gap_start_advertising(&adv_params);

            if(result != ESP_OK) {
                ESP_LOGW(TAG, "error starting to advertise! code: %i", result);
            }
        }
        break;

        case ESP_GATTS_CONGEST_EVT:
            ESP_LOGW(TAG, "ESP_GATTS_CONGEST_EVT");
            // if(param->congest.congested) {
            //     instance->charInst[0].sending = false;
            // } else {
            //     instance->charInst[0].sending = true;
            // }
            break;

        default:
            break;
    }
}
void BleTask::gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    // If event is register event, store the gatts_if for each profile
    if(event == ESP_GATTS_REG_EVT) {
        ESP_LOGI(TAG, "gatts_event_handler with app_id: %i and if: %i", param->reg.app_id, gatts_if);

        if(param->reg.status == ESP_GATT_OK) {
            bleTask->gl_profile_tab[param->reg.app_id].gatts_if = gatts_if;
        } else {
            ESP_LOGI(TAG, "Reg app failed, app_id %04x, status %d", param->reg.app_id, param->reg.status);
            return;
        }
    }

    // call the gatts callback with the profile index
    for(int i = 0; i < PROFILE_NUM; i++) {
        if(gatts_if == ESP_GATT_IF_NONE || gatts_if == bleTask->gl_profile_tab[i].gatts_if) {
            //ESP_LOGI(BLETASK_TAG, "-----> calling GATTS handler with interface %i and event %i", gatts_if, event);
            bleTask->gatts_profile_event_handler(event, gatts_if, param, i);
        }
    }
}
