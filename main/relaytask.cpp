#include "relaytask.h"

RelayTask::RelayTask(PidMemory *mem) : memory(mem) {
    memory->isBrewingManual = false;
    memory->isBrewingAuto = false;
}

void RelayTask::start() {
    gpio_config_t io_conf;
    // init relay output
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_RELAY_OUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init pump output
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_PUMP_OUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init valve output
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_VALVE_OUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init brew switch
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_BREW_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init water switch
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_WATER_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init steam switch
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_STEAM_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init button 1
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_BUTTON1_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init button 2
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_BUTTON2_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // start heater task
    xTaskCreate(run, "relayTask", 1024 * 2, this, configMAX_PRIORITIES, NULL);
    xTaskCreate(inoutTask, "inoutTask", 1024 * 2, this, configMAX_PRIORITIES, NULL);
    xTaskCreate(autoTask, "autoTask", 1024 * 2, this, configMAX_PRIORITIES, NULL);
}

void RelayTask::autoTask(void *arg) {
    RelayTask *task = reinterpret_cast<RelayTask*>(arg);
    bool *isBrewingAuto = &task->memory->isBrewingAuto;

    while(1) {
        if(*isBrewingAuto) {
            // auto process...
            ESP_LOGW("AUTO", "start auto with prebrew");
            task->setPumpEnabled(true);
            task->setValveEnabled(true);
            task->waitAbortable(pdMS_TO_TICKS(task->memory->secsPrebrewPump * 1000), isBrewingAuto);

            if(*isBrewingAuto) {
                ESP_LOGW("AUTO", "wait prebrew");
                task->setPumpEnabled(false);
                task->waitAbortable(pdMS_TO_TICKS(task->memory->secsPrebrewWait * 1000), isBrewingAuto);

                if(*isBrewingAuto) {
                    ESP_LOGW("AUTO", "start brew");
                    task->setPumpEnabled(true);
                    task->waitAbortable(pdMS_TO_TICKS(task->memory->secsBrewAuto * 1000), isBrewingAuto);
                }
            }

            ESP_LOGW("AUTO", "stop brew");
            task->setPumpEnabled(false);
            task->setValveEnabled(false);
            task->memory->isBrewingAuto = false;
        }

        vTaskDelay(pdMS_TO_TICKS(20));
    }
}

void RelayTask::waitAbortable(int ticks, bool *waitEnabled) {
    int tickStep = 10; // 100ms
    int steps = ticks / tickStep;

    for(int i = 0; i < steps; i++) {
        if(*waitEnabled) {
            vTaskDelay(tickStep);
        }
    }
}

void RelayTask::run(void *arg) {
    RelayTask *task = reinterpret_cast<RelayTask*>(arg);
    int relayPeriodMS = 5000;
    vTaskDelay(100);
    const float onWhilePump = 0.25; // 25% on, tastes too hot if higher

    while(1) {
        relayPeriodMS = task->pumpEnabled ? 1000 : 5000;
        float output = task->memory->output; // 0 - 1000 permil
        bool canHeatWhilePumping = task->memory->temperature < (task->memory->setpointBrew + 10.0);

        if(task->pumpEnabled && canHeatWhilePumping) {
            output = 1000 * onWhilePump;
        }

        int onMS = int(output * relayPeriodMS / 1000); // output is in permil

        // avoid too short heating time
        if(onMS > 100) {
            if(onMS < 250) {
                onMS = 250;
            }
        } else {
            onMS = 0;
        }

        int offMS = relayPeriodMS - onMS;
        ESP_LOGW("SSR", "times  %i / %i", onMS, offMS);
        ESP_LOGE("auto", "pre: %f, wait: %f, time: %f", task->memory->secsPrebrewPump, task->memory->secsPrebrewWait, task->memory->secsBrewAuto);

        if(onMS > 0) {
            task->setHeaterEnabled(true);
            task->waitAbortable(pdMS_TO_TICKS(onMS), &task->finishHeatingPeriod);
        }

        if(offMS > 0) {
            task->setHeaterEnabled(false);
            task->waitAbortable(pdMS_TO_TICKS(offMS), &task->finishHeatingPeriod);
        }

        task->finishHeatingPeriod = true;
    }
}

void RelayTask::inoutTask(void *arg) {
    RelayTask *task = reinterpret_cast<RelayTask*>(arg);

    while(1) {
        // read automatic mode button
        bool button1 = gpio_get_level(task->GPIO_BUTTON1_IN);

        if(task->stateButton1 != button1) {
            task->stateButton1 = button1;

            if(button1 == 0) {
                // button pressed, toggle auto state
                task->memory->isBrewingAuto = !task->memory->isBrewingAuto;
            }
        }

        if(task->memory->isBrewingAuto == false) {
            // read silvia buttons brew and water
            bool brewSet = gpio_get_level(task->GPIO_BREW_IN);
            bool waterSet = gpio_get_level(task->GPIO_WATER_IN);
            task->memory->steamOn = gpio_get_level(task->GPIO_STEAM_IN);
            //ESP_LOGI("buttonz", "brew: %i, water: %i, steam: %i", brewSet, waterSet, task->memory->steamOn);
            // some advanced logic goes here...
            bool enablePump = brewSet || waterSet;
            bool enableValve = brewSet;
            task->memory->isBrewingManual = brewSet;
            // assign outputs
            task->setPumpEnabled(enablePump);
            task->setValveEnabled(enableValve);
        }

        vTaskDelay(pdMS_TO_TICKS(20)); // 20ms = 50Hz
    }
}

void RelayTask::setHeaterEnabled(bool enabled) {
    ESP_LOGI("Heater", "%s", enabled ? "ON" : "OFF");
    gpio_set_level(GPIO_RELAY_OUT, enabled);
    memory->heaterOn = enabled;
}

void RelayTask::setPumpEnabled(bool enabled) {
    if(pumpEnabled != enabled) {
        ESP_LOGI("Pump", "%s", enabled ? "ON" : "OFF");
        gpio_set_level(GPIO_PUMP_OUT, enabled);
        pumpEnabled = enabled;
        finishHeatingPeriod = false;
    }
}

void RelayTask::setValveEnabled(bool enabled) {
    if(valveEnabled != enabled) {
        ESP_LOGI("Valve", "%s", enabled ? "ON" : "OFF");
        gpio_set_level(GPIO_VALVE_OUT, enabled);
        valveEnabled = enabled;
    }
}
