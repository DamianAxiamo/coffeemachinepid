#ifndef PIDMEMORY_H
#define PIDMEMORY_H

#include <stdio.h>

struct PidMemory {
    float p;
    float i;
    float d;
    float setpointBrew;
    float setpointSteam;
    float temperature;
    float output;
    bool heaterOn;
    bool steamOn;
    float secsPrebrewPump;
    float secsPrebrewWait;
    float secsBrewAuto;
    bool isBrewingManual;
    bool isBrewingAuto;
};

#endif // PIDMEMORY_H
