#include "maintask.h"

MainTask::MainTask() : sensor(), controller(&memory), bleTask(&memory, &requestSaveParameters), relayTask(&memory), display(&memory) {
}

void MainTask::start() {
    xTaskCreate(run, "mainTask", 1024 * 4, this, configMAX_PRIORITIES, NULL);
}

void MainTask::run(void *arg) {
    MainTask *task = reinterpret_cast<MainTask*>(arg);
    task->readValues();
    task->sensor.start(GPIO_NUM_22);
    task->bleTask.start();
    task->relayTask.start();
    task->display.initialize();
    task->memory.setpointSteam = 120.0;
    const int LOOP_DELAY_MS = 1000;

    while(1) {
        float temp = 150.0;

        if(!task->sensor.getTemperature(temp)) {
            ESP_LOGE("mainTask", "Error reading temperature!");
        }

        task->memory.temperature = temp;
        float setpoint = task->memory.steamOn ? task->memory.setpointSteam : task->memory.setpointBrew;
        task->controller.updatePID(setpoint, task->memory.temperature, LOOP_DELAY_MS / 1000.0);
        ESP_LOGI("PID", "Setpoint: %.1f\tTemperature: %.1f\tOutput: %.1f", task->memory.setpointBrew, task->memory.temperature, task->memory.output);
        vTaskDelay(pdMS_TO_TICKS(LOOP_DELAY_MS));

        if(task->requestSaveParameters) {
            task->requestSaveParameters = false;
            ESP_LOGI("Flash", "saving to flash...");
            task->writeValues();
        }
    }
}

void MainTask::readValues() {
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    ESP_ERROR_CHECK(err);
    size_t floatSize = 4;

    if(err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        err = nvs_get_blob(my_handle, "valueP", &memory.p, &floatSize);
        err = nvs_get_blob(my_handle, "valueI", &memory.i, &floatSize);
        err = nvs_get_blob(my_handle, "valueD", &memory.d, &floatSize);
        err = nvs_get_blob(my_handle, "valuePreBrew", &memory.secsPrebrewPump, &floatSize);
        err = nvs_get_blob(my_handle, "valuePreWait", &memory.secsPrebrewWait, &floatSize);
        err = nvs_get_blob(my_handle, "valueBrewTime", &memory.secsBrewAuto, &floatSize);
        err = nvs_get_blob(my_handle, "setpoint", &memory.setpointBrew, &floatSize);

        switch(err) {
            case ESP_OK:
                //printTimes();
                break;

            case ESP_ERR_NVS_NOT_FOUND:
                printf("The value is not initialized yet!\n");
                memory.p = 100.0;
                memory.i = 0;
                memory.d = 100.0;
                memory.setpointBrew = 30.0;
                memory.secsPrebrewPump = 3.0;
                memory.secsPrebrewWait = 3.0;
                memory.secsBrewAuto = 25.0;
                break;

            default :
                printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        // Close
        nvs_close(my_handle);
    }
}

void MainTask::writeValues() {
    nvs_handle_t my_handle;
    size_t floatSize = 4;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    ESP_ERROR_CHECK(err);

    if(err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        err = nvs_set_blob(my_handle, "valueP", &memory.p, floatSize);
        err = nvs_set_blob(my_handle, "valueI", &memory.i, floatSize);
        err = nvs_set_blob(my_handle, "valueD", &memory.d, floatSize);
        err = nvs_set_blob(my_handle, "valuePreBrew", &memory.secsPrebrewPump, floatSize);
        err = nvs_set_blob(my_handle, "valuePreWait", &memory.secsPrebrewWait, floatSize);
        err = nvs_set_blob(my_handle, "valueBrewTime", &memory.secsBrewAuto, floatSize);
        err = nvs_set_blob(my_handle, "setpoint", &memory.setpointBrew, floatSize);
        nvs_close(my_handle);
    }
}
