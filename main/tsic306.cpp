#include "tsic306.h"

Tsic306::Tsic306() {
}

void Tsic306::start(gpio_num_t rxPin) {
    const uart_config_t uart_config = {
        .baud_rate = 92000,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_REF_TICK,
    };
    uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, UART_PIN_NO_CHANGE, rxPin, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    xTaskCreate(rx_task, "uart_rx_task", 1024 * 4, this, 10, NULL);
}

void Tsic306::rx_task(void *arg) {
    Tsic306 *sensor = reinterpret_cast<Tsic306*>(arg);
    uint8_t rxBuffer[RX_BUF_SIZE];
    int rxBytes = 0;

    while(1) {
        rxBytes = uart_read_bytes(UART_NUM_1, rxBuffer, 20, pdMS_TO_TICKS(50));

        if(rxBytes > 0) {
            ESP_LOGE("SYNC", "not found, got %i bytes in 50ms", rxBytes);
            vTaskDelay(pdMS_TO_TICKS(5));
        } else {
            // pauseFound or no data
            rxBytes = uart_read_bytes(UART_NUM_1, rxBuffer, 20, pdMS_TO_TICKS(120));

            if(rxBytes == 20) {
                //ESP_LOGE("PAUSE", "found, got 20 bytes!");
                uint16_t word = 0;
                uint8_t byte = 0;

                // high byte
                if(sensor->parseFrame(rxBuffer, &byte)) {
                    if(byte <= 0b00000111) { // only least 3 bits can be set, otherwise wrong
                        word = byte;
                        word <<= 8;

                        // low byte
                        if(sensor->parseFrame(&rxBuffer[10], &byte)) {
                            word |= byte;
                            word &= 0x7FF; // mask 11bit valid data
                            sensor->temperature = sensor->toCelsius(word);
                            //ESP_LOGI("Parse", "Temp: %.1f °C", sensor->temperature);
                            sensor->lastReading = xTaskGetTickCount();
                        }
                    }
                }
            } else {
                ESP_LOGE("SYNC", "not found, got %i bytes instead of 20!", rxBytes);
            }
        }
    }
}

Tsic306::Symbol Tsic306::parseSymbol(uint8_t data) {
    switch(data) {
        case 0b00000000:
        case 0b10000000:
        case 0b11000000:
            return SYM_ZERO;

        case 0b11100000:
        case 0b11110000:
        case 0b11111000:
            return SYM_SYNC;

        case 0b11111100:
        case 0b11111110:
        case 0b11111111:
            return SYM_ONE;

        default:
            ESP_LOGE("Parse", "Invalid symbol: 0x%2x", data);
            return SYM_ERROR;
    }
}
float Tsic306::toCelsius(uint16_t data) {
    return (data * 200) / 2047.0 - 50;
}
bool Tsic306::getTemperature(float & temp) {
    bool valid = (xTaskGetTickCount() - lastReading) < pdMS_TO_TICKS(1000);

    if(valid) {
        ESP_LOGI("Temp", "%.2f", temperature);
        temp = temperature;
    }

    return valid;
}
bool Tsic306::parseFrame(uint8_t *data, uint8_t *result) {
    uint8_t value = 0;
    int ones = 0;

    // check frame consistency. first byte must be sync
    if(parseSymbol(data[0]) == SYM_SYNC) {
        // parse all 8 data bits
        for(int i = 1; i < 9; i++) {
            Tsic306::Symbol s = parseSymbol(data[i]);

            if(s != SYM_ERROR) {
                value <<= 1;

                if(s == SYM_ONE) {
                    value |= 1;
                    ones++;
                }
            } else {
                ESP_LOGE("Parse", "wrong symbol!");
                return false;
            }
        }

        // parse parity bit
        if(parseSymbol(data[9]) == SYM_ONE) {
            ones++;
        }

        // check parity
        if(ones & 0x01) {
            ESP_LOGE("Parse", "parity error!");
            esp_log_buffer_hex("Data", data, 10);
            return false;
        } else {
            //esp_log_buffer_hex("Data", data, 10);
        }
    } else {
        ESP_LOGW("Parse", "out of sync!");
        return false;
    }

    *result = value;
    return true;
}
