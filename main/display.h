#ifndef DISPLAY_H
#define DISPLAY_H

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "pidmemory.h"

extern "C" {
#include "tft.h"
#include "spiffs_vfs.h"
}

// ==========================================================
// Define which spi bus to use TFT_VSPI_HOST or TFT_HSPI_HOST
#define SPI_BUS TFT_HSPI_HOST
// ==========================================================

class Display {
public:
    Display(PidMemory *mem);
    void initialize();
private:
    void disp_header(char *info);
    bool spiffsMounted = false;
    PidMemory *memory;
    static void run(void *arg);
    void updateHeaterState(bool on);
};

#endif // DISPLAY_H
