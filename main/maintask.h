#ifndef MAINTASK_H
#define MAINTASK_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include "nvs_flash/include/nvs_flash.h"
#include "nvs_flash/include/nvs.h"
#include "tsic306.h"
#include "pid.h"
#include "bletask.h"
#include "relaytask.h"
#include "pidmemory.h"
#include "display.h"

class MainTask {
public:
    MainTask();
    void start();

private:
    PidMemory memory;
    Tsic306 sensor;
    PID controller;
    BleTask bleTask;
    RelayTask relayTask;
    Display display;
    static void run(void *arg);
    void writeValues();
    void readValues();

    bool requestSaveParameters = false;
};

#endif // MAINTASK_H
