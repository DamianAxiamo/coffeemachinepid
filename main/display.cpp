#include "display.h"

Display::Display(PidMemory *mem) : memory(mem) {
}

void Display::initialize() {
    esp_err_t ret;
    // === SET GLOBAL VARIABLES ==========================
    // ===================================================
    // ==== Set maximum spi clock for display read    ====
    //      operations, function 'find_rd_speed()'    ====
    //      can be used after display initialization  ====
    tft_max_rdclock = 8000000;
    // ===================================================
    // ====================================================================
    // === Pins MUST be initialized before SPI interface initialization ===
    // ====================================================================
    TFT_PinsInit();
    // ====  CONFIGURE SPI DEVICES(s)  ====================================================================================
    spi_lobo_device_handle_t spi;
    spi_lobo_bus_config_t buscfg = {
        .mosi_io_num = PIN_NUM_MOSI,            // set SPI MOSI pin
        .miso_io_num = PIN_NUM_MISO,            // set SPI MISO pin
        .sclk_io_num = PIN_NUM_CLK,             // set SPI CLK pin
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 6 * 1024,
    };
    spi_lobo_device_interface_config_t devcfg = {
        .mode = 0,                              // SPI mode 0
        .clock_speed_hz = 8000000,              // Initial clock out at 8 MHz
        .spics_io_num = -1,                     // we will use external CS pin
        .spics_ext_io_num = PIN_NUM_CS,         // external CS pin
        .flags = LB_SPI_DEVICE_HALFDUPLEX,      // ALWAYS SET  to HALF DUPLEX MODE!! for display spi
    };
    // ====================================================================================================================
    vTaskDelay(500 / portTICK_RATE_MS);
    printf("\r\n==============================\r\n");
    printf("TFT display DEMO, LoBo 11/2017\r\n");
    printf("==============================\r\n");
    printf("Pins used: miso=%d, mosi=%d, sck=%d, cs=%d\r\n", PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK, PIN_NUM_CS);
    printf("==============================\r\n\r\n");
    // ==================================================================
    // ==== Initialize the SPI bus and attach the LCD to the SPI bus ====
    ret = spi_lobo_bus_add_device(SPI_BUS, &buscfg, &devcfg, &spi);
    assert(ret == ESP_OK);
    printf("SPI: display device added to spi bus (%d)\r\n", SPI_BUS);
    tft_disp_spi = spi;
    // ==== Test select/deselect ====
    ret = spi_lobo_device_select(spi, 1);
    assert(ret == ESP_OK);
    ret = spi_lobo_device_deselect(spi);
    assert(ret == ESP_OK);
    printf("SPI: attached display device, speed=%u\r\n", spi_lobo_get_speed(spi));
    printf("SPI: bus uses native pins: %s\r\n", spi_lobo_uses_native_pins(spi) ? "true" : "false");
    // ================================
    // ==== Initialize the Display ====
    printf("SPI: display init...\r\n");
    TFT_display_init();
#ifdef TFT_START_COLORS_INVERTED
    TFT_invertDisplay(1);
#endif
    printf("OK\r\n");
#if USE_TOUCH == TOUCH_TYPE_STMPE610
    stmpe610_Init();
    vTaskDelay(10 / portTICK_RATE_MS);
    uint32_t tver = stmpe610_getID();
    printf("STMPE touch initialized, ver: %04x - %02x\r\n", tver >> 8, tver & 0xFF);
#endif
    // ---- Detect maximum read speed ----
    tft_max_rdclock = find_rd_speed();
    printf("SPI: Max rd speed = %u\r\n", tft_max_rdclock);
    // ==== Set SPI clock used for display operations ====
    spi_lobo_set_speed(spi, DEFAULT_SPI_CLOCK);
    printf("SPI: Changed speed to %u\r\n", spi_lobo_get_speed(spi));
    tft_font_rotate = 0;
    tft_text_wrap = 0;
    tft_font_transparent = 0;
    tft_font_forceFixed = 0;
    tft_gray_scale = 0;
    TFT_setGammaCurve(DEFAULT_GAMMA_CURVE);
    TFT_setRotation(LANDSCAPE);
    TFT_setFont(DEFAULT_FONT, NULL);
    TFT_resetclipwin();
    spiffsMounted = vfs_spiffs_register();
    //
    tft_fg = TFT_WHITE;
    tft_bg = TFT_BLACK;
    TFT_jpg_image(CENTER, CENTER, 0, SPIFFS_BASE_PATH"/images/test3.jpg", NULL, 0);
    vTaskDelay(100);
    xTaskCreate(run, "displayTask", 1024 * 4, this, configMAX_PRIORITIES, NULL);
}

void Display::run(void *arg) {
    Display *task = reinterpret_cast<Display*>(arg);
    TFT_fillScreen(tft_bg);
    task->disp_header("DSR PID");
    tft_fg = TFT_CYAN;
    TFT_resetclipwin();
    TFT_setFont(DEJAVU24_FONT, NULL);
    int fontHeight = TFT_getfontheight();
    int topMargin = 40;
    int spacing = 5;
    uint8_t y1 = topMargin;
    uint8_t y2 = topMargin + 1 * (spacing + fontHeight);
    uint8_t y3 = topMargin + 2 * (spacing + fontHeight);
    int xMargin = 10;
    TFT_print("Setpoint", xMargin, y1);
    TFT_print("Temp", xMargin, y2);
    TFT_print("Output", xMargin, y3);
    char text1[20];
    char text2[20];
    char text3[20];
    char text[20];
    int xStart = 120; // from here we need to clear old text residuals on test change
    int xEnd = 240 - xMargin;
    int count = 0;
    bool heaterOn = false;
    bool brewOn = false;
    int brewTimeMs = 0;
    const int loopMS = 50;
    TickType_t t;
    float autoDurationTotalMs = 0;
    int totalContentWidth = 240 - 2 * xMargin;

    while(1) {
        t = xTaskGetTickCount();
        // handle brew timer label
        bool brewNow = task->memory->isBrewingManual || task->memory->isBrewingAuto;

        if(brewNow != brewOn) {
            brewOn = brewNow;

            if(brewOn) {
                // reset brewTime
                brewTimeMs = 0;

                if(task->memory->isBrewingManual) {
                    TFT_print("\rTimer", xMargin, y3);
                }

                if(task->memory->isBrewingAuto) {
                    TFT_print("\r", xMargin, y3);
                    autoDurationTotalMs = 1000 * (task->memory->secsPrebrewPump + task->memory->secsPrebrewWait + task->memory->secsBrewAuto);
                }
            } else {
                vTaskDelay(100);
                TFT_print("\rOutput", xMargin, y3);
            }
        }

        if(brewOn) {
            // 3rd line brew timer
            brewTimeMs += loopMS;
        }

        if(++count == 10 || task->memory->isBrewingAuto) {
            count = 0;
            // update display slow data
            sprintf(text, "%.0f C", task->memory->steamOn ? task->memory->setpointSteam : task->memory->setpointBrew);

            if(strcmp(text, text1) != 0) {
                TFT_print("\r", xStart, y1);
                TFT_print(text, xEnd - TFT_getStringWidth(text), y1);
                memcpy(text1, text, 20);
            }

            sprintf(text, "%.0f C", task->memory->temperature);

            if(strcmp(text, text2) != 0) {
                TFT_print("\r", xStart, y2);
                TFT_print(text, xEnd - TFT_getStringWidth(text), y2);
                memcpy(text2, text, 20);
            }

            if(!brewOn) {
                // 3rd line heater output
                sprintf(text, "%.0f %%", (task->memory->output / 10.0));
            } else {
                if(task->memory->isBrewingManual) {
                    sprintf(text, "%i s", (brewTimeMs / 1000));
                }

                if(task->memory->isBrewingAuto) {
                    TFT_fillRect(xMargin, y3, (brewTimeMs * totalContentWidth) / autoDurationTotalMs, 25, TFT_WHITE);
                }
            }

            if(!task->memory->isBrewingAuto && strcmp(text, text3) != 0) {
                TFT_print("\r", xStart, y3);
                TFT_print(text, xEnd - TFT_getStringWidth(text), y3);
                memcpy(text3, text, 20);
            }
        }

        if(heaterOn != task->memory->heaterOn) {
            heaterOn = task->memory->heaterOn;
            task->updateHeaterState(heaterOn);
        }

        vTaskDelayUntil(&t, pdMS_TO_TICKS(loopMS));
    }
}

void Display::updateHeaterState(bool on) {
    TFT_fillCircle(223, 14, 8, on ? TFT_RED : TFT_BLACK);
}

void Display::disp_header(char *info) {
    TFT_fillScreen(TFT_BLACK);
    TFT_resetclipwin();

    if(tft_width < 240) TFT_setFont(DEF_SMALL_FONT, NULL);
    else TFT_setFont(DEJAVU18_FONT, NULL);

    TFT_fillRect(0, 0, tft_width - 1, TFT_getfontheight() + 8, tft_bg);
    TFT_drawRect(0, 0, tft_width - 1, TFT_getfontheight() + 8, TFT_DARKCYAN);
    TFT_print(info, CENTER, 4);
    TFT_setclipwin(0, TFT_getfontheight() + 9, tft_width - 1, tft_height - TFT_getfontheight() - 10);
}
