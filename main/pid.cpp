/*
  PID.cpp - Library for implementing a PID control loop. Used to ensure engines don't overshoot when reaching target roll/pitch.
  Created by Myles Grant <myles@mylesgrant.com>
  Based on: http://www.arduino.cc/playground/Main/BarebonesPIDForEspresso#pid
  See also: https://github.com/grantmd/QuadCopter

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "pid.h"

PID::PID(PidMemory *memory) : memory(memory) {
    iState = 0;
    last = 0;
}

void PID::updatePID(float target, float cur, float deltaTime) {
    // these local variables can be factored out if memory is an issue,
    // but they make it more readable
    float error;
    float windupGuard;
    // determine how badly we are doing
    error = target - cur;
    // the pTerm is the view from now, the pgain judges
    // how much we care about error at this instant.
    pTerm = memory->p * error;

    // iState keeps changing over time; it's
    // overall "performance" over time, or accumulated error
    if(memory->i > 0) {
        iState += error * deltaTime;
    } else {
        iState = 0;
    }

    // to prevent the iTerm getting huge despite lots of
    //  error, we use a "windup guard"
    // (this happens when the machine is first turned on and
    // it cant help be cold despite its best efforts)
    // not necessary, but this makes windup guard values
    // relative to the current iGain
    windupGuard = WINDUP_GUARD_GAIN / memory->i;

    if(iState > windupGuard) {
        iState = windupGuard;
    } else if(iState < -windupGuard) {
        iState = -windupGuard;
    }

    iTerm = memory->i * iState;
    // the dTerm, the difference between the temperature now
    //  and our last reading, indicated the "speed,"
    // how quickly the temp is changing. (aka. Differential)
    dTerm = (memory->d * (cur - last)) / deltaTime;
    // now that we've use lastTemp, put the current temp in
    // our pocket until for the next round
    last = cur;
    ESP_LOGI("PID", "pTerm: %.1f, iTerm: %.1f, dTerm: %.1f", pTerm, iTerm, dTerm);
    // the magic feedback bit
    float out = pTerm + iTerm - dTerm;

    if(out < 0.0) {
        out = 0.0;
    } else if(out > 1000.0) {
        out = 1000.0;
    }

    memory->output = out;
}

void PID::resetError() {
    iState = 0;
}
