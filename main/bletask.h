#ifndef _BLETASK_H_
#define _BLETASK_H_

// *** INCLUDES ***
// ****** STL
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ****** FreeRTOS
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/ringbuf.h"


// ****** ESP-IDF
#include "esp_system.h"
#include "esp_log.h"
#include "bt/include/esp_bt.h"
#include "bt/host/bluedroid/api/include/api/esp_gap_ble_api.h"
#include "bt/host/bluedroid/api/include/api/esp_gatt_common_api.h"
#include "bt/host/bluedroid/api/include/api/esp_gatts_api.h"
#include "bt/host/bluedroid/api/include/api/esp_bt_defs.h"
#include "bt/host/bluedroid/api/include/api/esp_bt_main.h"

#include "pidmemory.h"

// *** DEFINES ***

#ifdef CONFIG_BLUEDROID_PINNED_TO_CORE
#define BLUETOOTH_TASK_PINNED_TO_CORE               (CONFIG_BLUEDROID_PINNED_TO_CORE < portNUM_PROCESSORS ? CONFIG_BLUEDROID_PINNED_TO_CORE : tskNO_AFFINITY)
#else
#define BLUETOOTH_TASK_PINNED_TO_CORE               (0)
#endif

#define GATTS_SERVICE_UUID_USERDATA                 0x181C
#define GATTS_CHAR_UUID_USERDATA                    0x2A9A
#define GATTS_NUM_HANDLE_USERDATA                   4

#define PREPARE_BUF_MAX_SIZE                        256
#define adv_config_flag      (1 << 0)
#define scan_rsp_config_flag (1 << 1)

#define PROFILE_NUM                                 2
#define PROFILE_INDEX_USERDATA                      0
#define BT_RX_RINGBUF_LEN                           512


static uint8_t adv_service_uuid128[32] = {
    /* LSB <--------------------------------------------------------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xEE, 0x00, 0x00, 0x00,
    //second uuid, 32bit, [12], [13], [14], [15] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
};

class BleTask {
public:
    BleTask(PidMemory *memory, bool *requestSaveParameters);

    bool start();
    bool stop();

private:
    bool sendParameters = false;
    PidMemory *memory = nullptr;
    bool *requestSaveParameters = nullptr;
    struct prepare_type_env_t {
        uint8_t prepare_buf[PREPARE_BUF_MAX_SIZE];
        int prepare_len;
    } prepareWriteBuffer;

    RingbufHandle_t btRxBuff_h;
    uint8_t btRxByteBuff[BT_RX_RINGBUF_LEN];
    StaticRingbuffer_t btRxBuff;

    TaskHandle_t rxTask_h;
    static void rxTask(void *arg);
    TaskHandle_t txTask_h;
    static void txTask(void *arg);

    struct gatts_char_inst {
        uint16_t char_handle;
        esp_bt_uuid_t char_uuid;
        esp_gatt_perm_t char_perm;
        esp_gatt_char_prop_t char_prop;
        uint16_t descr_handle;
        esp_bt_uuid_t descr_uuid;
        bool sending;
    };

#define MAX_NUM_CHARS 1

    struct gatts_profile_inst {
        uint16_t gatts_if;
        uint16_t app_id;
        uint16_t conn_id;
        uint16_t service_handle;
        esp_gatt_srvc_id_t service_id;
        gatts_char_inst charInst[MAX_NUM_CHARS];
        uint8_t numChars;
        bool connected;
        char name[15];
    };

    esp_ble_adv_params_t adv_params = {
        .adv_int_min        = 0x20,
        .adv_int_max        = 0x40,
        .adv_type           = ADV_TYPE_IND,
        .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
        .peer_addr          = { 0 },
        .peer_addr_type     = BLE_ADDR_TYPE_PUBLIC,
        .channel_map        = ADV_CHNL_ALL,
        .adv_filter_policy  = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
    };

    //adv data
    esp_ble_adv_data_t adv_data = {
        .set_scan_rsp = false,
        .include_name = true,
        .include_txpower = true,
        .min_interval = 0x0006, //slave connection min interval, Time = min_interval * 1.25 msec
        .max_interval = 0x000C, //slave connection max interval, Time = max_interval * 1.25 msec
        .appearance = 0x00,
        .manufacturer_len = 0,
        .p_manufacturer_data = NULL,
        .service_data_len = 0,
        .p_service_data = NULL,
        .service_uuid_len = 32,
        .p_service_uuid = adv_service_uuid128,
        .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
    };

    // scan response data
    esp_ble_adv_data_t scan_rsp_data = {
        .set_scan_rsp = true,
        .include_name = true,
        .include_txpower = true,
        .min_interval = 0x0006,
        .max_interval = 0x000C,
        .appearance = 0x00,
        .manufacturer_len = 0,
        .p_manufacturer_data = NULL,
        .service_data_len = 0,
        .p_service_data = NULL,
        .service_uuid_len = 32,
        .p_service_uuid = adv_service_uuid128,
        .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
    };

    uint8_t adv_config_done = 0;

    static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);
    static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);

    void gatts_profile_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param, int index);
    void gattsSendResponse(esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);

    /* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
    gatts_profile_inst gl_profile_tab[PROFILE_NUM];
};

#endif // _BLETASK_H_
