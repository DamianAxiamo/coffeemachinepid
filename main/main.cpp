/* silvia PID by damian.weber@axiamo.com
 * CC0 license
*/
#include "maintask.h"

#include "nvs_flash/include/nvs.h"
#include "nvs_flash/include/nvs_flash.h"
#include "esp_err.h"

MainTask task;

int spiffs_is_registered = 0;
int spiffs_is_mounted = 0;

extern "C" void app_main(void) {
    esp_err_t ret = nvs_flash_init();

    if(ret == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    ESP_ERROR_CHECK(ret);
    task.start();
}
