#ifndef RELAYTASK_H
#define RELAYTASK_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include "driver/gpio.h"
#include "pidmemory.h"
#include "esp_log.h"



class RelayTask {
public:
    RelayTask(PidMemory *mem);
    void start();

private:
    //outputs
    const gpio_num_t GPIO_RELAY_OUT = GPIO_NUM_21;      // heater solid state relay output
    const gpio_num_t GPIO_PUMP_OUT = GPIO_NUM_13;       // pump solid state relay output (green)
    const gpio_num_t GPIO_VALVE_OUT = GPIO_NUM_12;      // valve solid state relay output (yellow)
    // inputs
    const gpio_num_t GPIO_BREW_IN = GPIO_NUM_17;        // brew switch
    const gpio_num_t GPIO_WATER_IN = GPIO_NUM_2;        // water switch
    const gpio_num_t GPIO_STEAM_IN = GPIO_NUM_15;       // steam switch
    const gpio_num_t GPIO_BUTTON1_IN = GPIO_NUM_0;      // button 1
    const gpio_num_t GPIO_BUTTON2_IN = GPIO_NUM_35;     // button 2
    PidMemory *memory;
    static void run(void *arg);
    void setHeaterEnabled(bool enabled);
    void setPumpEnabled(bool enabled);
    void setValveEnabled(bool enabled);
    static void inoutTask(void *arg);

    bool pumpEnabled = false;
    bool valveEnabled = false;

    bool finishHeatingPeriod = true;

    static void autoTask(void *arg);

    bool stateButton1 = false;
    bool stateButton2 = false;
    void waitAbortable(int ticks, bool *waitEnabled);
};

#endif // RELAYTASK_H
